import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import createAuth0Client, {
  GetTokenSilentlyOptions,
  GetUserOptions,
  LogoutOptions,
  RedirectLoginOptions,
  RedirectLoginResult
} from '@auth0/auth0-spa-js';
import {BehaviorSubject, from, Observable, Subscription} from 'rxjs';
import {concatMap, ignoreElements, map, shareReplay, tap} from 'rxjs/operators';

@Injectable()
export class AuthService {

  // Create an observable of Auth0 instance of client
  auth0Client$ = from(
    createAuth0Client({
      domain: 'my-domain',
      client_id: 'my-client-id',
      redirect_uri: `${window.location.origin}/user/callback`
    })
  )
    .pipe(
      shareReplay(1) // Every subscription receives the same shared value
    );

  private userProfileSubject = new BehaviorSubject<any>(null);

  private userSubscription: Subscription;

  constructor(
    private router: Router
  ) {
    this.userSubscription = this.getUser$()
      .subscribe(user => this.userProfileSubject.next(user));
  }

  /**
   * Performs a redirect to `/authorize` using the parameters
   * provided as arguments. Random and secure `state` and `nonce`
   * parameters will be auto-generated.
   */
  private loginWithRedirect$(options?: RedirectLoginOptions): Observable<void> {
    return this.auth0Client$.pipe(
      concatMap(client => from(client.loginWithRedirect(options)))
    );
  }

  /**
   * Clears the application session and performs a redirect to `/v2/logout`, using
   * the parameters provided as arguments, to clear the Auth0 session.
   * If the `federated` option is specified it also clears the Identity Provider session.
   * If the `localOnly` option is specified, it only clears the application session.
   * It is invalid to set both the `federated` and `localOnly` options to `true`,
   * and an error will be thrown if you do.
   * [Read more about how Logout works at Auth0](https://auth0.com/docs/logout).
   */
  private internalLogout$(options?: LogoutOptions): Observable<void> {
    return this.auth0Client$.pipe(
      tap(client => {
        client.logout(options);
      }),
      ignoreElements()
    );
  }

  /**
   * Returns the user information if available (decoded
   * from the `id_token`).
   */
  private getUser$(options?: GetUserOptions): Observable<any> {
    return this.auth0Client$.pipe(
      concatMap(client => from(client.getUser(options)))
    );
  }

  /**
   * Returns `true` if there's valid information stored,
   * otherwise returns `false`.
   */
  isAuthenticated$(): Observable<boolean> {
    return this.auth0Client$.pipe(
      concatMap(client => from(client.isAuthenticated()))
    );
  }

  /**
   * If there's a valid token stored, return it. Otherwise, opens an
   * iframe with the `/authorize` URL using the parameters provided
   * as arguments. Random and secure `state` and `nonce` parameters
   * will be auto-generated. If the response is successful, results
   * will be valid according to their expiration times.
   */
  getTokenSilently$(options?: GetTokenSilentlyOptions): Observable<string> {
    return this.auth0Client$.pipe(
      concatMap(client => from(client.getTokenSilently(options)))
    );
  }

  /**
   * After the browser redirects back to the callback page,
   * call `handleRedirectCallback` to handle success and error
   * responses from Auth0. If the response is successful, results
   * will be valid according to their expiration times.
   */
  handleRedirectCallback$(): Observable<RedirectLoginResult> {
    return this.auth0Client$.pipe(
      concatMap(client => from(client.handleRedirectCallback())),
      tap(_ => {
        this.userSubscription.unsubscribe();
        this.userSubscription = this.getUser$()
          .subscribe(user => this.userProfileSubject.next(user));
      })
    );
  }

  login$(redirectPath?: string): Observable<void> {
    // A desired redirect path can be passed to login method (e.g., from a route guard)
    return this.loginWithRedirect$({
      appState: {
        target: (redirectPath ? redirectPath : this.router.url)
      }
    });
  }

  logout$(): Observable<void> {
    return this.internalLogout$({
      returnTo: `${window.location.origin}/user/logout`
    })
      .pipe(
        tap(_ => {
          this.userSubscription.unsubscribe();
          this.userSubscription = this.getUser$()
            .subscribe(user => this.userProfileSubject.next(user));
        }),
        ignoreElements()
      );
  }

  get userProfile$(): Observable<any> {
    return this.userProfileSubject.asObservable();
  }

  get loggedIn$(): Observable<boolean> {
    return this.userProfile$
      .pipe(
        map(user => !!user)
      );
  }
}
