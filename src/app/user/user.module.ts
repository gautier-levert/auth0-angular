import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CallbackComponent} from './pages/callback/callback.component';
import {LogoutComponent} from './pages/logout/logout.component';

import {UserRoutingModule} from './user-routing.module';


@NgModule({
  declarations: [
    CallbackComponent,
    LogoutComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FlexLayoutModule
  ]
})
export class UserModule {
}
