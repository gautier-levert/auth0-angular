import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {concatMap, map} from 'rxjs/operators';
import {AuthService} from '../../../core/service/auth.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  constructor(
    private auth: AuthService,
    private router: Router
  ) {
  }

  ngOnInit(): void {

    // Call when app reloads after user logs in with Auth0
    const params = window.location.search;

    if (!params.includes('code=') || !params.includes('state=')) {
      this.router.navigateByUrl('/');
      return;
    }

    this.subscription.add(
      this.auth.handleRedirectCallback$()
        .pipe(
          map(cbRes => {
            // Get and set target redirect route from callback results
            return cbRes.appState && cbRes.appState.target ? cbRes.appState.target : '/';
          }),
          concatMap((targetRoute) => {
            // Redirect to target route after callback processing
            return this.router.navigateByUrl(targetRoute);
          })
        )
        .subscribe(
          _ => {
          },
          error => {
            console.error(error);
            this.router.navigateByUrl('/');
          }
        )
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
