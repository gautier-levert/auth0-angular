import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {AuthService} from '../../core/service/auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit, OnDestroy {

  loggedIn$: Observable<boolean>;

  private subscription: Subscription = new Subscription();

  constructor(
    private auth: AuthService
  ) {
  }

  ngOnInit() {
    this.loggedIn$ = this.auth.loggedIn$;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  login(): void {
    this.subscription.add(
      this.auth.login$()
        .subscribe()
    );
  }

  logout(): void {
    this.subscription.add(
      this.auth.logout$()
        .subscribe()
    );
  }
}
